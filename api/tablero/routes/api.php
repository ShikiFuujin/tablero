<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Rol', 'prefix' => '/rol'], function() {
  Route::get('/' , ['as' => 'rol', 'uses' => 'RolController@index']);
  Route::get('/{rol}' , ['as' => 'rol', 'uses' => 'RolController@show']);
  Route::put('/{rol}' , ['as' => 'rol', 'uses' => 'RolController@update']);
  // Route::delete('/{rol}' , ['as' => 'rol', 'uses' => 'RolController@delete']);
});

Route::group(['namespace' => 'Person', 'prefix' => '/person'], function() {
  Route::get('/' , ['as' => 'person', 'uses' => 'PersonController@index']);
  Route::get('/{person}' , ['as' => 'person', 'uses' => 'PersonController@show']);
  Route::put('/{person}' , ['as' => 'person', 'uses' => 'PersonController@update']);
  // Route::delete('/{person}' , ['as' => 'person', 'uses' => 'PersonController@delete']);
});

Route::post('/register' , ['as' => 'register', 'uses' => 'RegisterController@store']);

Route::group(['namespace' => 'Status', 'prefix' => '/status'], function() {
  Route::get('/' , ['as' => 'status', 'uses' => 'StatusController@index']);
  Route::get('/{status}' , ['as' => 'status', 'uses' => 'StatusController@show']);
  Route::post('/' , ['as' => 'status', 'uses' => 'StatusController@store']);
  Route::put('/{status}' , ['as' => 'status', 'uses' => 'StatusController@update']);
  // Route::delete('/{status}' , ['as' => 'status', 'uses' => 'StatusController@delete']);
});

Route::group(['namespace' => 'Project', 'prefix' => '/project'], function() {
  Route::get('/' , ['as' => 'project', 'uses' => 'ProjectController@index']);
  Route::get('/{project}' , ['as' => 'project', 'uses' => 'ProjectController@show']);
  Route::post('/' , ['as' => 'project', 'uses' => 'ProjectController@store']);
  Route::put('/{project}' , ['as' => 'project', 'uses' => 'ProjectController@update']);
  // Route::delete('/{status}' , ['as' => 'status', 'uses' => 'StatusController@delete']);
});

Route::group(['namespace' => 'Task', 'prefix' => '/task'], function() {
  Route::get('/' , ['as' => 'task', 'uses' => 'TaskController@index']);
  Route::get('/{task}' , ['as' => 'task', 'uses' => 'TaskController@show']);
  Route::post('/' , ['as' => 'task', 'uses' => 'TaskController@store']);
  Route::put('/{task}' , ['as' => 'task', 'uses' => 'TaskController@update']);

  Route::get('/project/{project}' , ['as' => 'task', 'uses' => 'TaskController@indexProject']);
  Route::get('/parent/{task}' , ['as' => 'task', 'uses' => 'TaskController@indexParent']);
  Route::get('/person/{task}' , ['as' => 'task', 'uses' => 'TaskController@getPersonByTaskList']);
  Route::get('/sub/{task}' , ['as' => 'task', 'uses' => 'TaskController@indexSub']);
  Route::post('/person' , ['as' => 'task', 'uses' => 'TaskController@storePersonTask']);
  Route::put('/person/{task}' , ['as' => 'task', 'uses' => 'TaskController@updatePersonTask']);
  Route::put('/person/delete/{task}' , ['as' => 'task', 'uses' => 'TaskController@deletePersonTask']);
  // Route::delete('/{status}' , ['as' => 'status', 'uses' => 'StatusController@delete']);
});

Route::group(['namespace' => 'Comment', 'prefix' => '/comment'], function() {
  Route::get('/' , ['as' => 'comment', 'uses' => 'CommentController@index']);
  Route::get('/task/{task}' , ['as' => 'comment', 'uses' => 'CommentController@indexTask']);
  Route::get('/{comment}' , ['as' => 'comment', 'uses' => 'CommentController@show']);
  Route::post('/' , ['as' => 'comment', 'uses' => 'CommentController@store']);
  Route::put('/{comment}' , ['as' => 'comment', 'uses' => 'CommentController@update']);
  // Route::delete('/{status}' , ['as' => 'status', 'uses' => 'StatusController@delete']);
});
