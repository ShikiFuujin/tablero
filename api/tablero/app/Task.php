<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';
    public $timestamps = false;
    protected $fillable = [
        'name', 'description','alias', 'initial_date','final_date', 'project','status', 'task_parent',
    ];
}
