<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Person;
use App\Http\Requests\UserRequest;

class RegisterController extends Controller {
  public function store(UserRequest $request){
    $listPerson = User::where('username', $request -> username) -> get();
    if($listPerson == null || sizeof($listPerson) <= 0){
      $person = new Person();
      $person  -> name = $request -> name;
      $person  -> email = $request -> email;
      $person -> save();

      $user = new User;
      $user -> username = $request -> username;
      $user -> password = bcrypt($request -> password);
      $user -> person = $person -> id;
      $user -> rol = $request -> rol;

      $user -> save();
      return response()->json($user, 201);
    }else{
      return response()->json(null, 409);
    }
  }
}
