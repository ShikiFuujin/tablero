<?php

namespace App\Http\Controllers\Rol;

use App\Rol;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Rol\StoreRolRequest;
use App\Http\Requests\Rol\UpdateRolRequest;

class RolController extends Controller
{
  public function index(){
    return Rol::all();
  }

  public function show(Rol $rol){
    return $rol;
  }

  public function store(StoreRolRequest $request){
    $rol = new Rol;
    $rol -> name  = $request -> name;
    $rol -> save();
    return response()->json($rol, 201);
  }

  public function update(UpdateRolRequest $request, Rol $rol){
    $rol->update($request->all());
    return response()->json($rol, 200);
  }

  public function delete(Rol $rol){
    $rol->delete();
    return response()->json(null, 204);
  }
}
