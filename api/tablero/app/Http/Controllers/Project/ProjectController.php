<?php

namespace App\Http\Controllers\Project;

use App\Project;
use App\Person;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Status\StatusController;
use App\Http\Requests\Project\StoreProjectRequest;
use App\Http\Requests\Project\UpdateProjectRequest;

class ProjectController extends Controller
{
  public function index(){
    $list = Project::all();
    foreach ($list as $value) {
      $value -> person_leader = Person::find($value -> person_leader);
      $value -> status = Status::find($value -> status);
    }
    return $list;
  }

  public function show(Project $project){
    return $project;
  }

  public function store(StoreProjectRequest $request){
    $createWithAlias = Project::where('alias', $request -> alias) -> count();

    if($createWithAlias > 0){
      return response()->json(null, 409);
    }

    $project = new Project;

    $project -> name  = $request -> name;
    $project -> description  = $request -> description;
    $project -> alias  = $request -> alias;
    $project -> initial_date  = date_create($request -> initial_date ? $request -> initial_date : '');
    $project -> final_date  = $request -> final_date ? date_create($request -> final_date) : null;
    $project -> person_leader  = $request -> person_leader;
    $project -> status = ((new StatusController) -> findByName(StatusController::STATUS_ACT)) -> id;

    $project -> save();
    return response()->json($project, 201);
  }

  public function update(UpdateProjectRequest $request, Project $project){

    $project -> name  = $request -> name;
    $project -> description  = $request -> description;
    $project -> initial_date  = date_create($request -> initial_date ? $request -> initial_date : '');
    $project -> final_date  = $request -> final_date ? date_create($request -> final_date) : null;
    $project -> person_leader  = $request -> person_leader;

    if($request -> status){
      $project -> status = (Status::find($request -> status)) -> id;
    }

    $project -> save();
    return response()->json($project, 200);
  }

  public function delete(Project $project){
    $project->delete();
    return response()->json(null, 204);
  }
}
