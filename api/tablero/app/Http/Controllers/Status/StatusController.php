<?php

namespace App\Http\Controllers\Status;

use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StatusResource;
use App\Http\Requests\Status\StoreStatusRequest;
use App\Http\Requests\Status\UpdateStatusRequest;

class StatusController extends Controller{

  public const STATUS_ACT = "Activado";
  public const STATUS_INA = "Inactivo";
  public const STATUS_PAU = "Pausado";

  public function index(){
    return Status::all();
  }

  public function show(Status $status){
    return $status;
  }

  public function findByName($status){
    return Status::where('name', $status) -> first();
  }

  public function store(StoreStatusRequest $request){
    $status = Status::create($request->all());
    return response()->json($status, 201);
  }

  public function update(UpdateStatusRequest $request, Status $status){
    $status->update($request->all());
    return response()->json($status, 200);
  }

  public function delete(Status $status){
    $status->delete();
    return response()->json(null, 204);
  }
}
