<?php

namespace App\Http\Controllers\Person;

use App\Person;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Person\UpdatePersonRequest;

class PersonController extends Controller
{
  public function index(){
    return Person::all();
  }

  public function show(Person $person){
    return $person;
  }

  public function update(UpdatePersonRequest $request, Person $person){
    $user = User::where('person' , $person -> id) -> first();
    $user -> password = bcrypt($request -> password);

    $person  -> name = $request -> name;
    $person  -> email = $request -> email;
    $person -> save();
    return response()->json($person, 200);
  }
}
