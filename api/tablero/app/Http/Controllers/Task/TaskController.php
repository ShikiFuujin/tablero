<?php

namespace App\Http\Controllers\Task;

use App\Task;
use App\Person;
use App\Status;
use App\Project;
use App\TaskHasPerson;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Status\StatusController;
use App\Http\Controllers\Comment\CommentController;
use App\Http\Controllers\Person\PersonController;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Http\Requests\TaskPerson\StoreTaskPersonRequest;
use App\Http\Requests\TaskPerson\UpdateTaskPersonRequest;

class TaskController extends Controller
{
  public function index(){
    return Task::all();
  }

  public function show(Task $task){
    $task -> sub = Task::where("task_parent", $task -> id) -> get();
    $task -> comments = ((new CommentController) -> indexTask($task));
    $task -> status = Status::find($task -> status);

    $listPerson = array();
    foreach ($this -> getPersonByTaskList($task) as $value_2) {
      array_push($listPerson, Person::find($value_2 -> person));
    }
    $task -> persons = $listPerson;

    foreach ($task -> sub as $value) {
      $listPerson = array();
      foreach ($this -> getPersonByTaskList($value) as $value_2) {
        array_push($listPerson, Person::find($value_2 -> person));
      }
      $value -> persons = $listPerson;
      $value -> status = Status::find($value -> status);
    }
    return $task;
  }

  public function store(StoreTaskRequest $request){
    $task = new Task;
    $task -> name  = $request -> name;
    $task -> description  = $request -> description;
    $task -> alias  = $request -> alias;
    $task -> initial_date  = date_create($request -> initial_date ? $request -> initial_date : '');
    $task -> final_date  = $request -> final_date ? date_create($request -> final_date) : null;
    $task -> project  = $request -> project;
    $task -> status = ((new StatusController) -> findByName(StatusController::STATUS_ACT)) -> id;
    $task -> task_parent = $request -> task_parent;

    $task -> save();

    if($task -> task_parent){
      $taskParent = new Task;
      $taskParent -> id = $task -> task_parent;

      $personTask = $this -> getPersonByTaskList($taskParent);
      foreach ($personTask as $value) {
        $stp = new StoreTaskPersonRequest;
        $stp -> task = $value -> task;
        $stp -> person = $value -> person;
        $this -> deletePersonTask($stp, $taskParent);
      }
    }
    return response()->json($task, 201);
  }

  public function update(UpdateTaskRequest $request, Task $task){
    $task -> name  = $request -> name;
    $task -> description  = $request -> description;
    $task -> initial_date  = date_create($request -> initial_date ? $request -> initial_date : '');
    $task -> final_date  = $request -> final_date ? date_create($request -> final_date) : null;
    if($request -> status){
      $task -> status = (Status::find($request -> status)) -> id;
    }
    $task -> save();

    return response()->json($task, 200);
  }

  public function delete(Task $task){
    $task->delete();
    return response()->json(null, 204);
  }

  public function indexParent(){

    $list = Task::where('task_parent', null) -> get();
    foreach ($list as $value) {
      $value -> status = Status::find($value -> status);
    }
    return $list;
  }

  public function indexSub(Task $task){
    $list = Task::where('task_parent', $task -> id) -> get();
    foreach ($list as $value) {
      $value -> status = Status::find($value -> status);
    }
    return $list;
  }

  public function indexProject(Project $project){
    $list = Task::where('project', $project -> id)
      -> where('task_parent', null)
      -> get();
    foreach ($list as $value) {
      $value -> status = Status::find($value -> status);
    }
    return $list;
  }

  public function updatePersonTask(UpdateTaskPersonRequest $request, Task $task){
    $statusUpdate = TaskHasPerson::where('person', $request -> person)
      -> where('task', $request -> task)
      -> update([
        'time_used' => $request -> time_used ?date("H:i:s",strtotime($request -> time_used)) : null
        , 'time_estimated' => $request -> time_estimated ? date("H:i:s",strtotime($request -> time_estimated)) : null
      ]);
    return response()->json($statusUpdate, $statusUpdate ? 201 : 200);
  }

  public function deletePersonTask(StoreTaskPersonRequest $request, Task $task){
    $statusUpdate = TaskHasPerson::where('person', $request -> person)
      -> where('task', $request -> task)
      -> delete();
    return response()->json($statusUpdate, $statusUpdate ? 201 : 200);
  }

  public function getPersonByTaskList(Task $task){
    return TaskHasPerson::where('task', $task -> id) -> get();
  }

  public function storePersonTask(StoreTaskPersonRequest $request){
      $task = new Task;
      $task -> id = $request -> task;
      $personTask = $this -> getPersonByTaskList($task);
      $yaReg = false;
      $taskHasPerson = new TaskHasPerson;
      foreach ($personTask as $value) {
        $yaReg = $value -> person == $request -> person;
        if($yaReg){
          $taskHasPerson = $value;
          break;
        }
      }
      if(!$yaReg){
        $taskHasPerson = new TaskHasPerson;
        $taskHasPerson -> task = $request -> task;
        $taskHasPerson -> person = $request -> person;
        $taskHasPerson -> save();
        return response()->json($taskHasPerson, 201);
      }else{
        return response()->json($taskHasPerson, 200);
      }
  }
}
