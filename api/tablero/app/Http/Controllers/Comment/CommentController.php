<?php

namespace App\Http\Controllers\Comment;

use App\Comment;
use App\Task;
use App\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\StoreCommentRequest;
use App\Http\Requests\Comment\UpdateCommentRequest;

class CommentController extends Controller
{
  public function index(){
    $list = Comment::all();
    foreach ($list as $value) {
      $value -> person = Person::find($value -> person);
    }
    return $list;
  }

  public function indexTask(Task $task){
    $list = Comment::where('task', $task -> id) -> where('comment_parent', null) -> get();
    foreach ($list as $value) {
      $value -> person = Person::find($value -> person);
      $value -> sub = Comment::where('comment_parent', $value -> id) -> get();
      foreach ($value -> sub as $valueSub) {
        $valueSub -> person = Person::find($valueSub -> person);
      }
    }
    return $list;
  }

  public function show(Comment $comment){
    $comment -> person = Person::find($comment -> person);
    return $comment;
  }

  public function store(StoreCommentRequest $request){
    $comment = new Comment;
    $comment -> title  = $request -> title;
    $comment -> comment  = $request -> comment;
    $comment -> comment_parent  = $request -> comment_parent;
    $comment -> comment_date  = date_create();
    $comment -> tags  = $request -> tags;
    $comment -> person  = (Person::first()) -> id;
    // $comment -> person  = $request -> person;
    $comment -> task  = $request -> task;

    $comment -> save();
    return response()->json($comment, 201);
  }

  public function update(UpdateCommentRequest $request, Comment $comment){
    $comment->update($request->all());
    return response()->json($comment, 200);
  }

  public function delete(Comment $comment){
    $comment->delete();
    return response()->json(null, 204);
  }
}
