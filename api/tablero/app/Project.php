<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    public $timestamps = false;
    protected $fillable = [
        'name', 'description','alias', 'initial_date','final_date', 'person_leader','status', 'avatar',
    ];
}
