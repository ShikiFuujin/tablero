<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskHasPerson extends Model
{
    protected $table = 'task_has_person';
    public $timestamps = false;
    protected $fillable = [
        'task', 'time_used','time_estimated', 'person'
    ];
}
