<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    public $timestamps = false;
    protected $fillable = [
      'title', 'comment','comment_parent', 'tags','person', 'comment_date', 'task'
    ];
}
