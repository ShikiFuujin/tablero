mainModule.factory('httpInterceptor', function($cookies, $timeout) {
  return {
    request: function(_obj) {
      return _obj;
    },
    requestError: function(_obj) {
      return _obj;
    },
    response: function(_obj) {
      return _obj;
    },
    responseError: function(_obj) {
      return _obj;
    }
  }
});

var $configStates = [];
var addConfigState = function(_name, _conf, _ids) {
  $configStates.push({
    name: _name,
    config: _conf,
    ids: _ids || []
  });
};
addConfigState('login', {
  url: '/login',
  templateUrl: "html/login.html"
})
addConfigState('inicio', {
  url: '/',
  templateUrl: "html/inicio.html"
})
addConfigState('projectSave', {
  url: '/projectSave',
  templateUrl: "html/project/save.html"
})
addConfigState('taskSave', {
  url: '/taskSave',
  templateUrl: "html/task/save.html"
})
addConfigState('taskProject', {
  url: '/taskProject',
  templateUrl: "html/task/byProject.html"
})
addConfigState('taskDetail', {
  url: '/taskDetail',
  templateUrl: "html/task/detail.html"
})
addConfigState('taskSubDetail', {
  url: '/taskSubDetail',
  templateUrl: "html/task/subDetail.html"
})
addConfigState('personSave', {
  url: '/personSave',
  templateUrl: "html/person/save.html"
})

mainModule.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  $httpProvider.interceptors.push('httpInterceptor');
  $urlRouterProvider.otherwise("/");
  $configStates.forEach(function(_item) {
    $stateProvider.state(_item.name, _item.config);
  })
})
mainModule.run(function($rootScope, $cookies, $location, $state, $timeout) {
  $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {

  });
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, options) {

  });
})
