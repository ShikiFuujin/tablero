Para cambios de IP o puerto del API de servicios, se deben ajustar los valores del aplicativo web y la app movil 
para que estos apunten a dichos servicios, para ello se ingresa en el archivo index.js ('web/public/' para el aplicativo web y
'app/tablero/www/' de la app movil) y se cambian los sgtes valores seg�n sea el caso:

    protocol: "http",
    ip: "localhost",
    port: "8000",
    context: "",
    enpoint: "api",