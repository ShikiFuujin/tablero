var $util = (function() {
  var a_ = {
    protocol: "http",
    ip: "localhost",
    port: "8000",
    context: "",
    enpoint: "api",
    web: false
  };
  var b_ = function(_a, _b) {
    _b.then(function(_resp) {
      _a({
        status: _resp.status == 200 || _resp.status == 201,
        data: _resp.data
      });
    }, function(_resp) {
      _a({
        status: false,
        data: _resp.data
      });
    })
  };
  return {
    objServices: a_,
    urlServices: a_.protocol + "://" + a_.ip + ":" + a_.port + "/" + (a_.context ? a_.context + "/" : "") + a_.enpoint,
    wrapService: b_
  }
})();

var mainModule = angular.module('mainModule', ["ngCookies", "ui.router"]);
