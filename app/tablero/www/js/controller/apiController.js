mainModule.controller("inicioCtrl", function($scope, $state, $timeout, registerFactory, rolFactory, projectFactory, taskFactory, commentFactory, statusFactory, personFactory) {

  $state.go("inicio");

  $scope.isWeb = $util.objServices.web;

  $scope.rolList = [];
  $scope.projectList = [];
  $scope.personList = [];
  $scope.taskList = [];
  $scope.statusList = [];
  $scope.taskSel = {};
  $scope.subTaskSel = {};
  $scope.errorMsj = "";

  $scope.projectSel =  {};
  $scope.projectSelSave = {};
  $scope.taskSelSave = {};
  $scope.taskPersonSelSave = {};
  $scope.personSave = {};

  $scope.commentSel = {};
  $scope.commentSubSel = {};

  var setError = function(_msj){
    if(_msj){
      $scope.errorMsj = _msj;
      $timeout(function() {
        $scope.errorMsj = "";
      }, 5000);
    }
  }

  var listStatus = function(_a) {
    $scope.statusList = [];
    statusFactory.list(function(_data){
      if(_data.status && _data.data){
        $scope.statusList = _data.data;
        _a(true);
      }else{
        _a(false);
      }
    });
  };

  var listRol = function(_a) {
    $scope.rolList = [];
    rolFactory.list(function(_data){
      if(_data.status && _data.data){
        $scope.rolList = _data.data;
        _a(true);
      }else{
        _a(false);
      }
    });
  };

  var listPerson = function(_a) {
    $scope.personList = [];
    personFactory.list(function(_data){
      if(_data.status && _data.data){
        $scope.personList = _data.data;
        _a(true);
      }else{
        _a(false);
      }
    });
  };

  var listProject = function(_a) {
    $scope.projectList = [];
    projectFactory.list(function(_data){
      if(_data.status && _data.data){
        $scope.projectList = _data.data;
        _a(true);
      }else{
        _a(false);
      }
    });
  };

  var listTaskProject = function(_a, _b) {
    $scope.taskList = [];
    taskFactory.byProject(function(_data){
      if(_data.status && _data.data){
        $scope.taskList = _data.data;
        _a(true);
      }else{
        _a(false);
      }
    }, _b);
  };

  var saveProject = function(_a, _b) {
    _b.person_leader = _b.person_leader.id;
    projectFactory[_b.id ? 'put' : 'post'](function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b, _b.id);
  };

  var saveTask = function(_a, _b) {
    taskFactory[_b.id ? 'put' : 'post'](function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b, _b.id);
  };

  var taskPerson = function(_a, _b) {
    taskFactory.taskPerson(function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b);
  };

  var deleteTaskPerson = function(_a, _b) {
    taskFactory.deleteTaskPerson(function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b, _b.task);
  };

  var listCommentTask = function(_a, _b) {
    commentFactory.byTask(function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a([]);
      }
    }, _b);
  };

  var getTask = function(_a, _b, _c) {
    _c = _c || false;
    $scope[!_c ? "taskSel" : "subTaskSel"] = {};
    taskFactory.get(function(_data){
      console.log(_data);
      if(_data.status && _data.data){
        $scope[!_c ? "taskSel" : "subTaskSel"] = _data.data;
        if(!_c){
            $scope.subTaskSel = {};
        }
        _a(true);
      }else{
        _a(false);
      }
    }, _b);
  };

  var saveComment = function(_a, _b) {
    commentFactory.post(function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b);
  };

  var saveUser = function(_a, _b) {
    _b.rol = _b.rol ? _b.rol.id : null;
    registerFactory.post(function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b);
  };

  var savePerson = function(_a, _b) {
    personFactory.put(function(_data){
      if(_data.status && _data.data){
        _a(_data.data);
      }else{
        _a(null);
      }
    }, _b, _b.id);
  };

  $scope.getTaskProject = function(_proj){
    $scope.projectSel = {};
    listTaskProject(function(_status) {
      console.log('listTaskProject' + _status);
      $scope.projectSel = angular.copy(_proj);
      $state.go("taskProject");
    }, _proj.id);
  };

  $scope.taskDetail = function(_task){
    var _sub = _task.task_parent || false;
    getTask(function(_status) {
      console.log("detalle tarea " + _status + " es sub " + _sub);
      if(!_sub){
        $state.go("taskDetail");
      }else{
        $state.go("taskSubDetail");
      }
    }, _task.id, _sub);
  };

  $scope.preEditProject = function(_proj){
    $scope.projectSelSave = {
      id: _proj.id
      , name: _proj.name
      , description: _proj.description
      , person_leader: _proj.person_leader
      , initial_date: _proj.initial_date ? new Date(_proj.initial_date) : null
      , final_date: _proj.final_date ? new Date(_proj.final_date) : null
    }
    $state.go("projectSave");
  }

  $scope.preEditTask = function(_task){
    $scope.taskSelSave = {
      id: _task.id
      , name: _task.name
      , description: _task.description
      , initial_date: _task.initial_date ? new Date(_task.initial_date) : null
      , final_date: _task.final_date ? new Date(_task.final_date) : null
      , task_parent: _task.task_parent
    }
    $state.go("taskSave");
  }

  $scope.saveSubTask = function(_task){
    $scope.taskSelSave = {
      task_parent: _task.id
    }
    $state.go("taskSave");
  }

  $scope.saveProject = function() {
    if($scope.projectSelSave && $scope.projectSelSave.person_leader){
      saveProject(function(_data) {
        if(_data)
          listProject(function(_status) {
            console.log("listProject " + _status);
            $scope.projectSelSave = {};
            $state.go("inicio");
          });
        else {
          setError("Hubo un error guardando datos");
        }
      }, angular.copy($scope.projectSelSave));
    }
  };

  $scope.changeStatusProj = function(_proj) {
    saveProject(function(_data) {
      if(_data){
        // listProject(function(_status) {
        //   console.log("listProject " + _status);
        //   $scope.projectSelSave = {};
        //   $state.go("inicio");
        // });
        console.log("Cambio estado");
      }else {
        setError("Hubo un error guardando datos");
      }
    }, {
      id: _proj.id
      , name: _proj.name
      , description: _proj.description
      , status: _proj.status.id
      , person_leader: _proj.person_leader
      , initial_date: _proj.initial_date ? new Date(_proj.initial_date) : null
      , final_date: _proj.final_date ? new Date(_proj.final_date) : null
    });
  }

  $scope.saveTask = function() {
    if($scope.taskSelSave){
      console.log($scope.taskSelSave);
      $scope.taskSelSave.project = $scope.projectSel.id;
      saveTask(function(_data) {
        if(_data)
          listTaskProject(function(_status) {
            console.log('listTaskProject' + _status);
            if($scope.taskSelSave.task_parent)
              $scope.taskDetail({
                id: $scope.taskSelSave.task_parent
              });
            else {
                $state.go("taskProject");
            }
            $scope.taskSelSave = {};
          }, $scope.projectSel.id);
        else {
          setError("Hubo un error guardando datos");
        }
      }, angular.copy($scope.taskSelSave));
    }
  };

  $scope.changeStatusTask = function(_task) {
    saveTask(function(_data) {
      if(_data){
        // listProject(function(_status) {
        //   console.log("listProject " + _status);
        //   $scope.projectSelSave = {};
        //   $state.go("inicio");
        // });
        console.log("Cambio estado");
      }else {
        setError("Hubo un error guardando datos");
      }
    }, {
      id: _task.id
      , name: _task.name
      , description: _task.description
      , status: _task.status.id
      , initial_date: _task.initial_date ? new Date(_task.initial_date) : null
      , final_date: _task.final_date ? new Date(_task.final_date) : null
    });
  }

  $scope.asignarTaskPerson = function(_task){
    taskPerson(function(_data) {
      console.log("asignarTaskPerson" , _data);
      $scope.taskPersonSelSave = {};
      $scope.taskDetail(_task);
    }, {
      task: _task.id,
      person: $scope.taskPersonSelSave.person.id
    })
  };

  $scope.eliminarTaskPerson = function(_task, _person){
    deleteTaskPerson(function(_data) {
      console.log("deleteTaskPerson" , _data);
      $scope.taskDetail(_task);
    }, {
      task: _task.id,
      person: _person.id
    })
  };

  $scope.nuevoComentario = function(_task, _comment){
    var _obj = {
      task: _task.id,
      title: _comment ? _comment.newTitle : $scope.commentSel.title,
      comment: _comment ? _comment.newComment : $scope.commentSel.comment,
      comment_parent: _comment ? _comment.id : null,
      person: 2
    };
    console.log(_obj);
    saveComment(function(_data) {
      console.log(_data);
      $scope.taskDetail(_task);
      $scope.commentSubSel = {};
      $scope.commentSel = {};
      // if(!(_task.task_parent || false)){
      //   $state.go("taskDetail");
      // }else{
      //   $state.go("taskSubDetail");
      // }
    }, _obj);
  };

  $scope.preEditPerson = function(_person) {
    $scope.personSave = {
      id: _person.id
      , name: _person.name
      , email: _person.email
    }
    $state.go("personSave");
  };

  $scope.savePerson = function() {
    if(!$scope.personSave.id){
      saveUser(function(_data) {
        listPerson(function(_status) {
          console.log("listPerson " + _status);
          $state.go("inicio");
        });
      }, angular.copy($scope.personSave));
    }
    else {
      savePerson(function(_data) {
        listPerson(function(_status) {
          console.log("listPerson " + _status);
          $state.go("inicio");
        });
      }, angular.copy($scope.personSave));
    }
  }

  listRol(function(_status) {
    console.log("listRolt " + _status);
  });

  listStatus(function(_status) {
    console.log("listStatus " + _status);
  });

  listPerson(function(_status) {
    console.log("listPerson " + _status);
  });

  listProject(function(_status) {
    console.log("listProject " + _status);
  });


});
