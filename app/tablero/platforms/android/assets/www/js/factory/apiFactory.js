mainModule.factory("statusFactory", function($http) {
  var $url = $util.urlServices + "/status";
  var a_ = function(_a) {
      $util.wrapService(_a, $http.get($url));
  };

  return{
    list: a_
  }
});

mainModule.factory("registerFactory", function($http) {
  var $url = $util.urlServices + "/register";
  var c_ = function(_a, _b) {
      $util.wrapService(_a, $http.post($url, _b));
  };
  return{
    post: c_
  }
});

mainModule.factory("personFactory", function($http) {
  var $url = $util.urlServices + "/person";
  var a_ = function(_a) {
      $util.wrapService(_a, $http.get($url));
  };
  var b_ = function(_a, _b) {
      $util.wrapService(_a, $http.get($url + "/" + _b));
  };
  var d_ = function(_a, _b, _c) {
      $util.wrapService(_a, $http.put($url  + "/" + _c, _b));
  };

  return{
    list: a_,
    get: b_,
    put: d_
  }
});

mainModule.factory("projectFactory", function($http) {
  var $url = $util.urlServices + "/project";
  var a_ = function(_a) {
      $util.wrapService(_a, $http.get($url));
  };
  var b_ = function(_a, _b) {
      $util.wrapService(_a, $http.get($url + "/" + _b));
  };
  var c_ = function(_a, _b) {
      $util.wrapService(_a, $http.post($url, _b));
  };
  var d_ = function(_a, _b, _c) {
      $util.wrapService(_a, $http.put($url  + "/" + _c, _b));
  };

  return{
    list: a_,
    get: b_,
    post: c_,
    put: d_
  }
});


mainModule.factory("taskFactory", function($http) {
  var $url = $util.urlServices + "/task";
  var a_ = function(_a) {
      $util.wrapService(_a, $http.get($url));
  };
  var b_ = function(_a, _b) {
      $util.wrapService(_a, $http.get($url + "/" + _b));
  };
  var c_ = function(_a, _b) {
      $util.wrapService(_a, $http.post($url, _b));
  };
  var d_ = function(_a, _b, _c) {
    $util.wrapService(_a, $http.put($url  + "/" + _c, _b));
  };
  var e_ = function(_a, _b) {
      $util.wrapService(_a, $http.get($url + "/project/" + _b));
  };
  var f_ = function(_a, _b) {
      $util.wrapService(_a, $http.post($url + "/person", _b));
  };
  var g_ = function(_a, _b,_c) {
      $util.wrapService(_a, $http.put($url + "/person/delete/"+_c, _b));
  };

  return{
    list: a_,
    get: b_,
    post: c_,
    put: d_,
    byProject: e_,
    taskPerson: f_,
    deleteTaskPerson: g_
  }
});

mainModule.factory("commentFactory", function($http) {
  var $url = $util.urlServices + "/comment";
  var a_ = function(_a) {
      $util.wrapService(_a, $http.get($url));
  };
  var b_ = function(_a, _b) {
      $util.wrapService(_a, $http.get($url + "/" + _b));
  };
  var c_ = function(_a, _b) {
      $util.wrapService(_a, $http.post($url, _b));
  };
  var d_ = function(_a, _b, _c) {
      $util.wrapService(_a, $http.put($url  + "/" + _b, _c));
  };
  var e_ = function(_a, _b) {
      $util.wrapService(_a, $http.get($url + "/task/" + _b));
  };

  return{
    list: a_,
    get: b_,
    post: c_,
    put: d_,
    byTask: e_
  }
});


mainModule.factory("rolFactory", function($http) {
  var $url = $util.urlServices + "/rol";
  var a_ = function(_a) {
      $util.wrapService(_a, $http.get($url));
  };
  return{
    list: a_
  }
});
